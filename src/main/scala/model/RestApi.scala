package model

import java.time.ZonedDateTime

object RestApi {

  trait Requests
  case class StartCollectRowData(exp: Option[String]) extends Requests
  case object StopCollectRowData extends Requests
  case class GetTwits(from: ZonedDateTime, to: ZonedDateTime, limit: Int) extends Requests
  case class GetTwitsByQuery(from: ZonedDateTime, to: ZonedDateTime, limit: Int, query: String) extends Requests




  trait Responces
  trait Err extends Responces
  case object Ok extends Responces
  case class AnyErr(message: String) extends Err
  case class Twits(data: List[Twit]) extends Responces {
    override def toString: String = data.mkString("\n")
  }
  case class TwitsData(data: List[TwitData]) extends Responces

}
