package model

import java.time.ZonedDateTime


case class Twit(message: String) {
  override def toString: String = message
}

case class TwitData(time: ZonedDateTime, twit: String)


