package model

import org.joda.time.DateTime

import common.DateTimeUtils._


case class TwitCassandra(surrogate_pk: String, time: DateTime, twit: String) {
  def toTwitData = TwitData(time.toZonedDateTime, twit)
}
