package routs

import akka.actor.Props
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.util.Timeout
import akka.routing._

import scala.concurrent.duration._

import actors.DbActor
import model.RestApi.{GetTwits, GetTwitsByQuery}
import data.Repository


class DbRout(implicit repository: Repository) extends BaseRout {

  import common.AkkaImplicits._
  import common.RestApiImplicits._

  val timeout10Minutes = 10.minute
  implicit val timeout: Timeout = timeout10Minutes

  val dbActor = system.actorOf(
    Props(new DbActor).withRouter(RoundRobinPool(nrOfInstances = 7)),
    "dbActor")

  val getRoute = pathPrefix("db") {

    path("get-twits") {
      post {
        withRequestTimeout(timeout10Minutes) {
          entity(as[GetTwits]) { request =>
            onComplete(dbActor ? request)(makeResult)
          }
        }
      }
    } ~
      path("get-twits-by-query") {
        post {
          withRequestTimeout(timeout10Minutes) {
            entity(as[GetTwitsByQuery]) { request =>
              onComplete(dbActor ? request)(makeResult)
            }
          }
        }
      }

  }

}