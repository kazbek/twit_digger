package routs

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Route, StandardRoute}

import scala.util.{Failure, Success, Try}

import model.RestApi.{AnyErr, Twits, TwitsData}


trait BaseRout {

  import common.RestApiImplicits._

  protected val makeResult: (Try[Any]) => StandardRoute = {
    case Success(value) => value match {
      case err: AnyErr => complete(StatusCodes.BadRequest, err)
      case twits : Twits => complete(StatusCodes.Accepted, twits)
      case twitsData : TwitsData => complete(StatusCodes.Accepted, twitsData)
      case _ => complete(StatusCodes.Accepted, "Ok")
    }
    case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
  }

  def getRoute: Route

}
