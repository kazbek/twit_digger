package routs


import akka.actor.Props
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.Source
import akka.util.{ByteString, Timeout}

import scala.concurrent.duration._

import model.RestApi.{StartCollectRowData, Twits}
import actors.BackpressuredActor

class TwitRout extends BaseRout {

  import common.AkkaImplicits._
  implicit val timeout: Timeout = 5.seconds

  val getRoute = pathPrefix("twit") {

    path("row") {
      get {
        val actorRef = system.actorOf(Props[BackpressuredActor])
        val publisher = ActorPublisher[Twits](actorRef)
        val source = Source.fromPublisher(publisher).map(twits => ByteString(twits + "\n"))
        actorRef ! StartCollectRowData(None)
        complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, source))
      }
    } ~
    path("contains" / RemainingPath) { exp =>
      get {
        val actorRef = system.actorOf(Props[BackpressuredActor])
        val publisher = ActorPublisher[Twits](actorRef)
        val source = Source.fromPublisher(publisher).map(twits => ByteString(twits + "\n"))
        actorRef ! StartCollectRowData(Some(exp.toString))
        complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, source))
      }
    }

  }


}