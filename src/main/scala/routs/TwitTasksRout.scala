package routs

import akka.actor.Props
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._

import model.RestApi.{StartCollectRowData, StopCollectRowData}
import actors.TwitTasksActor

class TwitTasksRout extends BaseRout {

  import common.AkkaImplicits._
  implicit val timeout: Timeout = 10.seconds

  val twitTasksActor = system.actorOf(Props(new TwitTasksActor), "twitTasksActor")

  val getRoute = pathPrefix("twit-tasks") {

    path("start") {
      get {
        onComplete(twitTasksActor ? StartCollectRowData)(makeResult)
      }
    } ~
      path("stop") {
        get {
          onComplete(twitTasksActor ? StopCollectRowData)(makeResult)
        }
      }

  }

}