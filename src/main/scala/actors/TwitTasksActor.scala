package actors

import akka.actor.{Actor, ActorLogging}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Time
import com.datastax.spark.connector.SomeColumns
import com.datastax.spark.connector._

import data.stream.{KafkaStreamingProducer, TwitStreamingContext}
import data.KuduContextHelper
import model.RestApi.{Ok, StartCollectRowData, StopCollectRowData}
import common.DateTimeUtils._
import common.Settings._

class TwitTasksActor extends Actor with ActorLogging with TwitStreamingContext with KafkaStreamingProducer with KuduContextHelper {

  private def flow(rdd: RDD[String], time: Time): Unit = {
    if (rdd.count() > 0) {
      //rdd.foreach(str => println(time.toSurrogate_pk_s + " >>> >>> >>> " + str))
      rdd.cache()
      rdd.map(str => (time.toSurrogate_pk_s, time.milliseconds, str))
        .saveToCassandra(cassandraKeyspace, cassandraTable, SomeColumns("surrogate_pk", "time", "twit"))

      // TODO: uncommit when kudu environment will be ready
      //insertToKudu(time, rdd)

      // Collect DStream Data
      //rdd.collect().foreach(sendToKafka)
    }
  }

  def receive: Receive = {

    case StartCollectRowData => {
      log.info(s"StartCollectRowData comes.")
      start(flow)
      sender ! Ok
    }

    case StopCollectRowData => {
      log.info(s"StopCollectRowData comes.")
      stop
      sender ! Ok
    }

  }

}