package actors

import akka.actor.ActorLogging
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.{Cancel, Request}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Time

import scala.annotation.tailrec
import scala.language.postfixOps

import data.stream.KafkaStreamingContext
import model.RestApi.{StartCollectRowData, StopCollectRowData, Twits}
import model.Twit


class BackpressuredActor extends ActorPublisher[Twits] with ActorLogging with KafkaStreamingContext {

  val MaxBufferSize = 1000
  var buffer = Vector.empty[Twits]
  implicit val ec = context.dispatcher

  @tailrec
  private def deliverBuffer(): Unit =
    if (totalDemand > 0 && isActive) {
      // You are allowed to send as many elements as have been requested by the stream subscriber
      // total demand is a Long and can be larger than what the buffer has
      if (totalDemand <= Int.MaxValue) {
        val (sendDownstream, holdOn) = buffer.splitAt(totalDemand.toInt)
        buffer = holdOn
        // send the stuff downstream
        sendDownstream.foreach(onNext)
      } else {
        val (sendDownStream, holdOn) = buffer.splitAt(Int.MaxValue)
        buffer = holdOn
        sendDownStream.foreach(onNext)
        // recursive call checks whether is more demand before repeating the process
        deliverBuffer()
      }
    }


  private def flow(f: String => Boolean)(rdd: RDD[String], time: Time): Unit = {
    if (rdd.count() > 0) {
      val collect = rdd.filter(f).collect()
      onNext(Twits(collect.toList.map(Twit)))
    }
  }

  override def receive: Receive = {

    case StartCollectRowData(exp) =>
      start(flow(exp match {
        case None => _ => true
        case Some(str) => (text) => text.contains(str)
      }))

    case StopCollectRowData =>
      stop

    case _: Twits if buffer.size == MaxBufferSize =>
      log.warning("received a Twits message when the buffer is maxed out")
      sender() ! "CommandDenied"

    case twits: Twits =>
      sender() ! "CommandAccepted"
      if (buffer.isEmpty && totalDemand > 0 && isActive) {
        // send elements to the stream immediately since there is demand from downstream and we
        // have not buffered anything so why bother buffering, send immediately
        // You send elements to the stream by calling onNext
        onNext(twits)
      }
      else {
        // there is no demand from downstream so we will store the result in our buffer
        // Note that :+= means add to end of Vector
        buffer :+= twits
      }

    // A request from down stream to send more data
    // When the stream subscriber requests more elements the ActorPublisherMessage.Request message is
    // delivered to this actor, and you can act on that event. The totalDemand is updated automatically.
    case Request(_) => deliverBuffer()

    // When the stream subscriber cancels the subscription the ActorPublisherMessage.Cancel message is
    // delivered to this actor. If the actor is stopped the stream will be completed, unless it was not
    // already terminated with failure, completed or canceled.
    case Cancel =>
      log.info("Stream was cancelled")
      stop
      context.stop(self)

    case other =>
      log.warning(s"Unknown message $other received")
  }

}