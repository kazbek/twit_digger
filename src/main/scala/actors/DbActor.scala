package actors

import akka.actor.{Actor, ActorLogging}

import model._
import RestApi._
import data.Repository


class DbActor(implicit repository: Repository) extends Actor with ActorLogging {

  def receive: Receive = {

    case request: GetTwits => {
      log.info(s"GetTwits comes.")
      sender ! repository.getTwits(request)
    }

    case request: GetTwitsByQuery => {
      log.info(s"GetTwitsByQuery comes.")
      sender ! repository.getTwitsByQuery(request)
    }

  }

}