package common

import org.apache.spark.sql.SparkSession

import common.SparkImplicits._


object SparkSessionImplicits {

  lazy val spark = SparkSession
    .builder()
    .config(conf)
    .enableHiveSupport()
    .getOrCreate()

}
