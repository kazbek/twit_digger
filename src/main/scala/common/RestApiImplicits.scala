package common


import spray.json.DefaultJsonProtocol._

import model.RestApi.{AnyErr, GetTwits, GetTwitsByQuery, Twits, TwitsData}
import common.TwitImplicits.{twitFormat, twitDataFormat}
import common.ZonedDateTimeProtocol.ZonedDateTimeJsonFormat


object RestApiImplicits {

  implicit val getTwitsDataFormat = jsonFormat3(GetTwits)
  implicit val getTwitsByQueryDataFormat = jsonFormat4(GetTwitsByQuery)
  implicit val anyErrFormat = jsonFormat1(AnyErr)
  implicit val twitsFormat = jsonFormat1(Twits)
  implicit val twitsDataFormat = jsonFormat1(TwitsData)

}
