package common

import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd.CassandraTableScanRDD

import model.TwitCassandra
import common.SparkImplicits._
import common.Settings._


object CassandraRepositoryExtentions {

  implicit def rdd = sc.cassandraTable[TwitCassandra](cassandraKeyspace, cassandraTable)
  implicit def toTwitCassandra(rdd: CassandraTableScanRDD[TwitCassandra]): CassandraTableScanRDD[TwitCassandra] =
    rdd.select("surrogate_pk", "time", "twit")

}
