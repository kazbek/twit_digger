package common

import com.typesafe.config.ConfigFactory


object Settings {

  private val config = ConfigFactory.load()

  val actorSystemName: String = config.getString("akka.actor-system")

  val cassandraAddress: String = config.getString("cassandra.master")
  val cassandraKeyspace: String = config.getString("cassandra.keyspace")
  val cassandraTable: String = config.getString("cassandra.table")

  val sparkAppName: String = config.getString("spark.app-name")
  val sparkMaster: String = config.getString("spark.master")
  val sparkDriverMemory: String = config.getString("spark.driver_memory")
  val sparkExecutorMemory: String = config.getString("spark.executor_memory")

  val kafkaBrokers: Array[String] = config.getStringList("kafka.brokers").toArray().map(_.toString)
  val kafkaTopics: Array[String] = config.getStringList("kafka.topics").toArray().map(_.toString)
  val zkQuorum: String = config.getString("kafka.zookeeper.quorum")
  val zkGroup: String = config.getString("kafka.zookeeper.group")
  val zkNumThreads: Int = config.getInt("kafka.zookeeper.num-threads")

  val kuduMaster: String = config.getString("kudu.master")
  val kuduTable: String = config.getString("kudu.table")

}
