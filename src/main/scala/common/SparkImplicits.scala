package common

import org.apache.spark.{SparkConf, SparkContext}

import common.Settings._


object SparkImplicits {

  val conf = new SparkConf()
    .setAppName(sparkAppName)
    .setMaster(sparkMaster)
    .set("spark.driver.memory", sparkDriverMemory)
    .set("spark.executor.memory", sparkExecutorMemory)
    .set("spark.cassandra.connection.host", cassandraAddress)
    .set("spark.driver.allowMultipleContexts", "false")

  implicit val sc = new SparkContext(conf)

  /** Makes sure only ERROR messages get logged to avoid log spam. */
  def setupLogging() = {
    import org.apache.log4j.{Level, Logger}
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)
  }

}
