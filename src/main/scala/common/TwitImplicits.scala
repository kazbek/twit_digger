package common

import spray.json.DefaultJsonProtocol._

import model.{Twit, TwitData}
import common.ZonedDateTimeProtocol._
import model.RestApi.TwitsData


object TwitImplicits {

  implicit class ExtentionTwitsData(val data: Array[TwitData]) {
    def toTwitsData = TwitsData(data.toList)
  }

  implicit val twitFormat = jsonFormat1(Twit)
  implicit val twitDataFormat = jsonFormat2(TwitData)

}
