package common

import java.time.{ZoneId, ZonedDateTime}
import java.util.Calendar

import org.apache.spark.streaming.Time
import org.joda.time.{DateTime, DateTimeZone}

object DateTimeUtils {

  /** Data in one Cassandra bucket have to be less than 200 mb
    * 1 twit = 140 characters = 560 8-bit bytes
    * additional data approx 40 bytes. Total twit row approx 600 bytes
    * there are approx 130 000 twits come during 1 hour
    * if partition key = yyyy-mm-day:hour the approx data amount = 130 000 * 600 bytes = 74 Mb (must be less than 200 Mb)
    * */
  def toPk(yyyy: Int, mm: Int, day: Int, hour: Int) =
    s"$yyyy-$mm-$day-$hour"


  implicit class ExtentionDateTime(val dt: DateTime) {
    def toZonedDateTime = dt.toGregorianCalendar.toZonedDateTime
  }

  implicit class ExtentionLong(val utc: Long) {
    def toZonedDateTime = new DateTime(utc, DateTimeZone.UTC).toZonedDateTime
  }


  implicit class ExtentionZonedDateTime(val zdt: ZonedDateTime) {
    def toDateTime = {
      val zone = DateTimeZone.forID(zdt.getZone.getId)
      new DateTime(zdt.toInstant.toEpochMilli, zone)
    }
    def getMillis = {
      zdt.toDateTime.getMillis
    }
  }

  def toSurrogate_pk_s(from: ZonedDateTime, to: ZonedDateTime) =
    (0L to (to.toDateTime.getMillis - from.toDateTime.getMillis) / (1000L * 60 * 60))
      .map(from.plusHours)
      .map(zdt => toPk(zdt.getYear, zdt.getMonth.getValue, zdt.getDayOfMonth, zdt.getHour)).toList


  implicit class ExtentionTime(val time: Time) {
    def toSurrogate_pk_s = {
      val calendar = Calendar.getInstance()
      calendar.setTimeInMillis(time.milliseconds)
      toPk(
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH) + 1, // The first month of the year in the Gregorian and Julian calendars is 0
        calendar.get(Calendar.DAY_OF_MONTH),
        calendar.get(Calendar.HOUR_OF_DAY))
    }
  }

  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)


}
