package common

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import common.Settings._


object AkkaImplicits {

  implicit val system = ActorSystem(actorSystemName)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

}
