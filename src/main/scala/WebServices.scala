import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Directives._

import scala.concurrent.Future

import data.Repository
import routs.{DbRout, TwitRout, TwitTasksRout}
import common.AkkaImplicits._

// Developer Monolith Service. Not for Production
class DeveloperMonolithService(implicit repository: Repository) {

  val twit = new TwitRout()
  val twitTasks = new TwitTasksRout()
  val db = new DbRout()
  val route = {
    twit.getRoute ~ twitTasks.getRoute ~ db.getRoute
  }

  import common.AkkaImplicits._

  private var bindingFuture: Future[ServerBinding] = null

  def bind(ip:String = "localhost", port : Int = 8081) =
    bindingFuture = Http().bindAndHandle(route, ip, port)

  def unbind =
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}

// TwitStream -> Sparkstream -> db (Cassandra & Kudu) & Kafka
class TwitCollectorService(implicit repository: Repository) {

  private var bindingFuture: Future[ServerBinding] = null

  def bind(ip:String = "localhost", port : Int = 8091) =
    bindingFuture = Http().bindAndHandle(new TwitTasksRout().getRoute, ip, port)

  def unbind =
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}

// Kafka -> SparkStream -> Akka & Akka Stream
class KafkaRealTimeProcessingService(implicit repository: Repository) {

  private var bindingFuture: Future[ServerBinding] = null

  def bind(ip:String = "localhost", port : Int = 8093) =
    bindingFuture = Http().bindAndHandle(new TwitRout().getRoute, ip, port)

  def unbind =
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}

// db (Cassandra & Kudu) -> Spark (DataSets, SQL, Hive and etc...) -> Akka & Akka Stream
class ArchiveDataLoaderService(implicit repository: Repository) {

  private var bindingFuture: Future[ServerBinding] = null

  def bind(ip:String = "localhost", port : Int = 8095) =
    bindingFuture = Http().bindAndHandle(new DbRout().getRoute, ip, port)

  def unbind =
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}