import data.{CassandraRepository, KuduRepository}

import scala.io.StdIn

case class EP(port: Int, ip: String = "localhost")

object EpHelper {

  implicit class ArgsHelper(val args: Array[String]) {
    def toEp(ep: EP): EP =
      if (args.length < 2) ep
    else {
      try
        EP(args(1).toInt, args(0))
      catch {
        case _: Throwable => ep
      }
    }
  }

}

import EpHelper.ArgsHelper

// Developer Monolith Service. Not for Production
object DeveloperMonolithWebServer extends App  {

  implicit val repository = new CassandraRepository
  val server = new DeveloperMonolithService()
  val ep = args.toEp(EP(8081))

  println(s"Server Start ${ep.ip}:${ep.port}")
  server.bind(ep.ip, ep.port)
  println("press any key for exit")
  StdIn.readLine()
  server.unbind
  println("Server End")

}

// TwitStream -> Sparkstream -> db (Cassandra & Kudu) & Kafka
object TwitCollectorWebServer extends App  {

  implicit val repository = new CassandraRepository
  val server = new TwitCollectorService()
  val ep = args.toEp(EP(8091))

  println(s"Server Start ${ep.ip}:${ep.port}")
  println("TwitStream -> Sparkstream -> db (Cassandra & Kudu) & Kafka")
  server.bind(ep.ip, ep.port)
//  println("press any key for exit")
//  StdIn.readLine()
//  server.unbind
//  println("Server End")

}

// Kafka -> SparkStream -> Akka & Akka Stream
object KafkaRealTimeProcessingWebServer extends App  {

  implicit val repository = new CassandraRepository
  val server = new KafkaRealTimeProcessingService()
  val ep = args.toEp(EP(8093))

  println(s"Server Start ${ep.ip}:${ep.port}")
  println("Kafka -> SparkStream -> Akka & Akka Stream")
  server.bind(ep.ip, ep.port)
//  println("press any key for exit")
//  StdIn.readLine()
//  server.unbind
//  println("Server End")

}

// Cassandra -> Spark (DataSets, SQL, Hive and etc...) -> Akka & Akka Stream
object CassandraDataLoaderWebServer extends App  {

  implicit val repository = new CassandraRepository
  val server = new ArchiveDataLoaderService()
  val ep = args.toEp(EP(8095))

  println(s"Server Start ${ep.ip}:${ep.port}")
  println("Cassandra -> Spark (DataSets, SQL, Hive and etc...) -> Akka & Akka Stream")
  server.bind(ep.ip, ep.port)
//  println("press any key for exit")
//  StdIn.readLine()
//  server.unbind
//  println("Server End")

}

// Kudu -> Spark (DataSets, SQL, Hive and etc...) -> Akka & Akka Stream
object KuduDataLoaderWebServer extends App  {

  implicit val repository = new KuduRepository
  val server = new ArchiveDataLoaderService()
  val ep = args.toEp(EP(8097))

  println(s"Server Start ${ep.ip}:${ep.port}")
  println("Kudu -> Spark (DataSets, SQL, Hive and etc...) -> Akka & Akka Stream")
  server.bind(ep.ip, ep.port)
//  println("press any key for exit")
//  StdIn.readLine()
//  server.unbind
//  println("Server End")

}