package data.stream

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Time
import org.apache.spark.streaming.twitter.TwitterUtils

import common.SparkImplicits._
import data.stream.TwitUtils.setupTwitter


trait TwitStreamingContext extends BaseStreamingContext {

  // Configure Twitter credentials using twitter.txt
  setupTwitter()

  ssc = getActiveOrCreate

  setupLogging()

  override def start(func: (RDD[String], Time) => Unit): Unit = {
    ssc = getActiveOrCreate

    TwitterUtils
      .createStream(ssc, None)
      .map(status => status.toString)
      .foreachRDD(func(_, _))

    ssc.start()
  }

  override def stop: Unit = {
    ssc.stop(stopSparkContext = false, stopGracefully = true)
  }

}
