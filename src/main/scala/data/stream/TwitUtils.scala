package data.stream


object TwitUtils {

  /** Configures Twitter service credentials using twiter.txt in the main workspace directory */
  def setupTwitter() = {
    scala.io.Source.fromFile("./src/main/resources/twitter.txt").getLines
      .map(_.split(" ")).filter(_.length == 2)
      .foreach(fields => System.setProperty("twitter4j.oauth." + fields(0), fields(1)))
  }

}
