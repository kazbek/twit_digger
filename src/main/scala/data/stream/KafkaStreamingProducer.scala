package data.stream

import java.util
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

import common.Settings._

// https://hub.docker.com/r/ches/kafka/
// docker run -d -p 2181:2181 -p 2888:2888 -p 3888:3888 --name zookeeper jplock/zookeeper:3.4.6
// docker run -d -p 7203:7203 -p 6667:6667 -p 9092:9092 --name kafka --link zookeeper:zookeeper ches/kafka

// >  Terminal 1
// $  ZK_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' zookeeper)
// $  KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafka)
// $  docker run --rm ches/kafka kafka-topics.sh --create --topic twit_row --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181
// $  docker run --rm --interactive ches/kafka kafka-console-producer.sh --topic twit_row --broker-list $KAFKA_IP:9092
//
// >  Terminal 2
// $  ZK_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' zookeeper)
// $  KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafka)
// $  docker run --rm ches/kafka kafka-console-consumer.sh --topic twit_row --from-beginning --zookeeper $ZK_IP:2181



// KAFKA LOCAL ON MAC OS
// Install Homebrew
// $    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
// https://dtflaneur.wordpress.com/2015/10/05/installing-kafka-on-mac-osx/
// $ brew search kafka
// $ brew install kafka
// $ zkserver start
// $ cd /usr/local/Cellar/kafka/0.10.1.0/bin
// $ kafka-server-start /usr/local/etc/kafka/server.properties
//     in diff terminal
// $ cd /usr/local/Cellar/kafka/0.10.1.0/bin
// $ kafka-console-consumer --zookeeper localhost:2181 --topic twit_row --from-beginning
// >>>>>>>  Recieve message from Kafka
//     in diff terminal
// $ cd /usr/local/Cellar/kafka/0.10.1.0/bin
// $ kafka-console-producer --broker-list localhost:9092 --topic twit_row
// >>>>>>>  Send messages to Kafka

// Setting up a multi-broker cluster
// cp config/server.properties config/server-1.properties
// cp config/server.properties config/server-2.properties
//config/server-1.properties:
//   broker.id=1
//   port=9093
//   log.dir=/tmp/kafka-logs-1
//
//config/server-2.properties:
//   broker.id=2
//   port=9094
//   log.dir=/tmp/kafka-logs-2
// bin/kafka-create-topic --zookeeper localhost:2181 --replica 3 --partition 1 --topic twit_row_replicated
// bin/kafka-list-topic --zookeeper localhost:2181
// bin/kafka-console-producer --broker-list localhost:9092 --topic twit_row_replicated
// bin/kafka-console-consumer --zookeeper localhost:2181 --from-beginning --topic twit_row_replicated

trait KafkaStreamingProducer {

  // Zookeeper connection properties
  val props = new util.HashMap[String, Object]()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers.mkString(","))
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
    "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
    "org.apache.kafka.common.serialization.StringSerializer")

  private val producer = new KafkaProducer[String, String](props)

  def sendToKafka(message: String) : Unit =
    producer.send(new ProducerRecord[String, String](kafkaTopics.head, null, message))

}
