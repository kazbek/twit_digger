package data.stream

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import common.Settings._
import common.SparkImplicits._
import org.apache.spark.streaming.dstream.ReceiverInputDStream


trait KafkaStreamingContext extends BaseStreamingContext {

  ssc = getActiveOrCreate

  setupLogging()

  override def start(func: (RDD[String], Time) => Unit): Unit = {
    ssc = getActiveOrCreate

    val topicMap = {
      kafkaTopics.map((_, zkNumThreads)).toMap
    }
    KafkaUtils
      .createStream(ssc, zkQuorum, zkGroup, topicMap)
      .map(_._2)
      .foreachRDD(func(_, _))

    //ssc.checkpoint("checkpoint")
    ssc.start()
  }

  override def stop: Unit = {
    ssc.stop(stopSparkContext = false, stopGracefully = true)
  }

}
