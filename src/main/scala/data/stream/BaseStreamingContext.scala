package data.stream

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}

import common.SparkImplicits.sc


trait BaseStreamingContext {

  def getActiveOrCreate = StreamingContext.getActiveOrCreate(() => new StreamingContext(sc, Seconds(1)))

  var ssc: StreamingContext = _

  def start(func: (RDD[String], Time) => Unit): Unit

  def stop: Unit

}
