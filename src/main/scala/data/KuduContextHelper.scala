package data

import org.apache.spark.sql.DataFrame
import org.apache.kudu.spark.kudu._
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Time

import common.Settings._
import common.SparkSessionImplicits._


trait KuduContextHelper {

  // Create Kudu DataFrame
  lazy val kuduDataFrame: DataFrame = spark.read.options(Map("kudu.master" -> kuduMaster, "kudu.table" -> kuduTable)).kudu

  // Use KuduContext to create, delete, or write to Kudu tables
  lazy val kuduContext = new KuduContext(kuduMaster)

  // Insert data
  import spark.implicits._

  def insertToKudu(time: Time, rdd: RDD[String]): Unit =
    kuduContext.insertRows(rdd.map(twit => (time, twit)).toDF, kuduTable)

}
