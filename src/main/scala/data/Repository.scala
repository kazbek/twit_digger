package data

import model.RestApi.{GetTwits, GetTwitsByQuery, TwitsData}


trait Repository {

  def getTwits(condition: GetTwits): TwitsData

  def getTwitsByQuery(condition: GetTwitsByQuery): TwitsData

}
