package data

import model.RestApi.{GetTwits, GetTwitsByQuery, TwitsData}
import model.TwitData

class FakeRepository extends Repository {
  override def getTwits(condition: GetTwits): TwitsData =
    TwitsData(List(TwitData(condition.from, "Fake Twit 1")))

  override def getTwitsByQuery(condition: GetTwitsByQuery): TwitsData =
    TwitsData(List(TwitData(condition.from, "Fake Twit 2")))
}
