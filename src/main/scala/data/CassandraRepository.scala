package data

import model.RestApi.{GetTwits, GetTwitsByQuery, TwitsData}
import common.DateTimeUtils._
import common.CassandraRepositoryExtentions._
import common.TwitImplicits._

// Docker
// https://hub.docker.com/_/cassandra/
// docker run --name cassy -d cassandra:tag
// CQLSH
// docker run -it --link cassy:cassandra --rm cassandra cqlsh cassandra

// Cassandra CQL
// CREATE KEYSPACE "twit_digger" WITH REPLICATION = { 'class' : 'SimpleStrategy' , 'replication_factor' :1 };
// CREATE TABLE twit_digger.twits (surrogate_pk varchar, time timestamp, twit varchar, PRIMARY KEY (surrogate_pk, time, twit));

// https://github.com/datastax/spark-cassandra-connector/blob/master/doc/2_loading.md

class CassandraRepository extends Repository {

  override def getTwits(condition: GetTwits): TwitsData = toTwitCassandra(rdd)
    .filter(row => toSurrogate_pk_s(condition.from, condition.to).contains(row.surrogate_pk) &&
      row.time.isAfter(condition.from.toDateTime) &&
      row.time.isBefore(condition.to.toDateTime))
    .take(condition.limit)
    .map(_.toTwitData).toTwitsData

  override def getTwitsByQuery(condition: GetTwitsByQuery): TwitsData = toTwitCassandra(rdd)
    .filter(row => toSurrogate_pk_s(condition.from, condition.to).contains(row.surrogate_pk) &&
      row.time.isAfter(condition.from.toDateTime) &&
      row.time.isBefore(condition.to.toDateTime) &&
      row.twit.contains(condition.query))
    .take(condition.limit)
    .map(_.toTwitData).toTwitsData

}
