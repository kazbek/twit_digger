package data


import java.util

import org.apache.kudu.client.CreateTableOptions
import org.apache.kudu.spark.kudu.KuduContext
import org.joda.time.{DateTimeZone, LocalDateTime}


import model.RestApi.{GetTwits, GetTwitsByQuery, TwitsData}
import common.SparkSessionImplicits._
import common.TwitImplicits._
import common.DateTimeUtils._
import model.TwitData

// Docker 1
// https://hub.docker.com/r/andreysabitov/impala-kudu/
// docker run -d -p 8050:8050 -p 8051:8051 -p 25000:25000 -p 25010:25010 -p 25020:25020 -p 50070:50070 -p 50075:50075 7050:7050 -p 7051:7051 andreysabitov/impala-kudu:latest
// List of ports:
//    http://www.cloudera.com/documentation/enterprise/latest/topics/cdh_ig_ports_cdh5.html
//    http://www.cloudera.com/content/www/en-us/documentation/enterprise/latest/topics/cm_ig_ports_impala.html
// docker ps ... -> get "container name"
// sudo docker exec -i -t "container name" /bin/bash

// Impala Shell
// root@a410d774cb22:/opt#  impala-shell
// kudu-master.example.com = data from Kudu Master UI (ex: http://localhost:8051/masters)
// http://kudu.apache.org/docs/kudu_impala_integration.html
//   CREATE TABLE twit_digger (time BIGINT, twit STRING)
//   DISTRIBUTE BY HASH (time) INTO 16 BUCKETS
//   TBLPROPERTIES( 'storage_handler' = 'com.cloudera.kudu.hive.KuduStorageHandler', 'kudu.table_name' = 'twit_digger',
//                  'kudu.master_addresses' = 'a410d774cb22:7051', 'kudu.key_columns' = 'time,twit', 'kudu.num_tablet_replicas' = '1');


// Docker 2
// docker pull kunickiaj/kudu
// docker run -d --name kudu-master -p 8051:8051 -p 7050:7050 -p 7051:7051 kunickiaj/kudu master
// docker run -d --name kudu-tserver -p 8050:8050 --link kudu-master -e KUDU_MASTER=kudu-master kunickiaj/kudu tserver
// docker logs -f kudu-master
// docker logs -f kudu-tserver
// docker run --rm -it --link kudu-tserver -e KUDU_TSERVER=kudu-tserver kunickiaj/kudu kudu tserver status kudu-tserver

class KuduRepositoryTemplate {

  import spark.implicits._
  case class twit_digger(time: Long, twit: String)
  val nowUTC = new LocalDateTime().toDateTime(DateTimeZone.UTC).getMillis
  val arr = Array(twit_digger(nowUTC, "Twit_1"), twit_digger(nowUTC, "Twit_2"))
  val rdd = spark.sparkContext.parallelize(arr)
  val df = rdd.toDF
  val list = new util.ArrayList[String]()
  list.add("time")

  // Use KuduContext to create, delete, or write to Kudu tables
  val kuduContext = new KuduContext("localhost:7051")

  // Create Table
  kuduContext.createTable(
    "twit_digger",
    df.schema,
    Seq("time", "twit"),
    new CreateTableOptions()
      .setNumReplicas(1)
      .addHashPartitions(list,4))

  // Insert Data
  kuduContext.insertRows(df, "twit_digger")
}

class KuduRepository extends Repository with KuduContextHelper {

  //  // Then query using spark api or register a temporary table and use spark sql
  //  kuduDataFrame.select("twit").filter("twit like '%Obama%'").show()

  // Register kuduDataFrame as a temporary table for spark-sql
  val viewName = "twit_digger_view"
  kuduDataFrame.createOrReplaceTempView(viewName)


  // Select from the dataframe
  override def getTwits(condition: GetTwits): TwitsData =
    spark.sqlContext.sql(
      s"select time, twit from $viewName where time >= ${condition.from.getMillis} and time < ${condition.to.getMillis} limit ${condition.limit}")
      .select("time", "twit").rdd.collect()
      .map(row => TwitData(row.getLong(0).toZonedDateTime, row.getString(1))).toTwitsData

  // Select from the dataframe
  override def getTwitsByQuery(condition: GetTwitsByQuery): TwitsData =
    spark.sqlContext.sql(
      s"select time, twit from $viewName where time >= ${condition.from.getMillis} and time < ${condition.to.getMillis} and twit like '% ${condition.query}%' limit ${condition.limit} ")
      .select("time", "twit").rdd.collect()
      .map(row => TwitData(row.getLong(0).toZonedDateTime, row.getString(1))).toTwitsData

}
