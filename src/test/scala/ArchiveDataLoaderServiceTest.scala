import akka.http.scaladsl.model.StatusCodes
import com.m3.curly._
import org.scalatest.{BeforeAndAfter, FunSuite}
import spray.json.JsonParser

import model.RestApi.TwitsData
import data.FakeRepository

class ArchiveDataLoaderServiceTest extends FunSuite with BeforeAndAfter  {

  implicit val repository = new FakeRepository
  val server = new ArchiveDataLoaderService()

  val ip = "localhost"
  val port = 8899
  server.bind(ip, port)

  import common.RestApiImplicits._


  test("get-twits") {
    val body = "{\"from\": \"2016-09-28T14:10:40+03:00[GMT+03:00]\", \"to\": \"2016-09-28T14:10:40+03:00[GMT+03:00]\", \"limit\": 100}"
    val response = HTTP.post(s"http://$ip:$port/db/get-twits", body.getBytes, "application/json")
    assert(StatusCodes.getForKey(response.getStatus).get === StatusCodes.Accepted)
    val responce = JsonParser(response.getTextBody).convertTo[TwitsData]
    assert(responce.data.head.twit === "Fake Twit 1")
  }

  test("get-twits-by-query") {
    val body = "{\"from\": \"2016-09-28T14:10:40+03:00[GMT+03:00]\", \"to\": \"2016-09-28T14:10:40+03:00[GMT+03:00]\", \"limit\": 100, \"query\": \"query string\"}"
    val response = HTTP.post(s"http://$ip:$port/db/get-twits-by-query", body.getBytes, "application/json")
    assert(StatusCodes.getForKey(response.getStatus).get === StatusCodes.Accepted)
    val responce = JsonParser(response.getTextBody).convertTo[TwitsData]
    assert(responce.data.head.twit === "Fake Twit 2")

    server.unbind
  }

}
