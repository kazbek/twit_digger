
name := "twit-digger"

version := "1.0"

scalaVersion := "2.11.8"

unmanagedBase <<= baseDirectory { base => base / "jars_external" }

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-core" % "2.4.5",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.5",
  "com.typesafe.akka" %% "akka-http-spray-json-experimental" % "2.4.5",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "com.m3" %% "curly-scala" % "0.5.+",

  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "org.apache.spark" %% "spark-catalyst" % "2.1.0",
  "org.apache.spark" %% "spark-hive" % "2.1.0",
  "org.apache.spark" %% "spark-streaming" % "2.1.0",
  "org.apache.spark" %% "spark-mllib" % "2.1.0",
  "org.apache.spark" %% "spark-graphx" % "2.1.0",

  "org.apache.bahir" %% "spark-streaming-twitter" % "2.0.0",
  "org.apache.spark" %% "spark-streaming-kafka-0-8" % "2.1.0",
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.0.0-M3"
)

//lazy val root = (project in file(".")).
//  settings(
//    name := "server",
//    version := "1.0",
//    scalaVersion := "2.11.8",
//    mainClass in Compile := Some("CassandraDataLoaderWebServer")
//  )

//assemblyMergeStrategy in assembly := {
//  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
//  case x => MergeStrategy.first
//}



// TwitCollectorWebServer, KafkaRealTimeProcessingWebServer, CassandraDataLoaderWebServer
mainClass in assembly := some("CassandraDataLoaderWebServer")
assemblyJarName := "CassandraDataLoaderWebServer.jar"
val meta = """META.INF(.)*""".r
assemblyMergeStrategy in assembly := {
  case PathList("javax", "servlet", xs @ _*) => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".html" => MergeStrategy.first
  case n if n.startsWith("reference.conf") => MergeStrategy.concat
  case n if n.endsWith(".conf") => MergeStrategy.concat
  case meta(_) => MergeStrategy.discard
  case x => MergeStrategy.first
}